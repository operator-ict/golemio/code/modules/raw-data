# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.1.6] - 2024-09-12

### Changed

-   `.gitlab-ci.yml` cleanup ([devops#320](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/issues/320))

## [1.1.5] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.1.4] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.1.3] - 2023-08-23

### Changed

-   Adding documentation ([raw-data#2](https://gitlab.com/operator-ict/golemio/code/modules/raw-data/-/issues/2))
-   Run integration apidocs tests via Golemio CLI ([core#46](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/46))

## [1.1.2] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [1.1.1] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.1.0] - 2023-01-23

### Changed

-   Migrate to npm

## [1.0.5] - 2022-11-29

### Changed

-   Update gitlab-ci template

## [1.0.4] - 2022-06-21

### Changed

-   Typescript version update from 4.4.4 to 4.6.4

