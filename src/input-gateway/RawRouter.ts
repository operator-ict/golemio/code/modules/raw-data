import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { checkContentTypeMiddleware, contentTypeToExtension } from "@golemio/core/dist/input-gateway";
const bodyParser = require("body-parser");

export class RawRouter {
    // Assign router to the express.Router() instance
    public router: Router = Router();

    constructor() {
        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        const plainTextParser = bodyParser.text();

        this.router.post(
            "/:provider_name",
            plainTextParser,
            checkContentTypeMiddleware(Object.keys(contentTypeToExtension)),
            this.Post
        );
    };

    private Post = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };
}

const rawRouter = new RawRouter().router;

export { rawRouter };
