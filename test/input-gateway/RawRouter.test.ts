import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import request from "supertest";
import express, { Request, Response, NextFunction } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler } from "@golemio/core/dist/shared/golemio-errors";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import { rawRouter } from "#ig/RawRouter";

const bodyParser = require("body-parser");
require("body-parser-xml")(bodyParser);

chai.use(chaiAsPromised);

describe("RawRouter", () => {
    // Create clean express instance
    const app = express();
    app.use(
        bodyParser.json({
            limit: "2MB", // Reject payload bigger than limit
        })
    );
    app.use(
        bodyParser.xml({
            limit: "2MB", // Reject payload bigger than limit
            xmlParseOptions: {
                explicitArray: false, // Only put nodes in array if >1
                normalize: true, // Trim whitespace inside text nodes
                normalizeTags: true, // Transform tags to lowercase
            },
        })
    );
    app.use(bodyParser.text());

    let testJsonData: any;
    let testTextData: string;
    let testXmlData: string;

    before(async () => {
        // Mount the tested router to the express instance
        app.use("/raw", rawRouter);
        app.use((err: any, _req: Request, res: Response, _next: NextFunction) => {
            const error = HTTPErrorHandler.handle(err, log);
            if (error) {
                res.setHeader("Content-Type", "application/json; charset=utf-8");
                res.status(error.error_status || 500).send(error);
            }
        });
        testJsonData = {
            neco: "nnneeecccooo JSON",
        };
        testTextData = "nnneeecccooo plain";
        testXmlData = "<neco>nnneeecccooo XML</neco>";
    });

    it("should respond with 204 to POST /raw/:providerName with JSON data", (done) => {
        request(app)
            .post("/raw/providerName")
            .send(testJsonData)
            .set("Content-Type", "application/json")
            .expect(204)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                done();
            });
    });

    it("should respond with 204 to POST /raw/:providerName with XML data", (done) => {
        request(app)
            .post("/raw/providerName")
            .send(testXmlData)
            .set("Content-Type", "application/xml")
            .expect(204)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                done();
            });
    });

    it("should respond with 204 to POST /raw/:providerName with plain text data", (done) => {
        request(app)
            .post("/raw/providerName")
            .send(testTextData)
            .set("Content-Type", "text/plain")
            .expect(204)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                done();
            });
    });

    it("should respond with 406 to POST /raw/:providerName with bad content type", (done) => {
        request(app)
            .post("/raw/providerName")
            .send("value=0") // x-www-form-urlencoded upload
            .expect(406)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                done();
            });
    });

    it("should respond with 406 to POST /raw/:providerName with no content type", (done) => {
        request(app)
            .post("/raw/providerName")
            .send(testTextData)
            .unset("Content-Type")
            .expect(406)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                done();
            });
    });
});
