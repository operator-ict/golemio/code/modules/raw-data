// Load reflection lib
require("@golemio/core/dist/shared/_global");

const path = require("path");
require("ts-node").register({ project: path.resolve(process.cwd(), "tsconfig.json") });
require("tsconfig-paths").register();

const http = require("http");
const express = require("express");

const app = express();
const server = http.createServer(app);

const bodyParser = require("body-parser");

const start = async () => {
    app.use(bodyParser.json());

    const { RawRouter } = require("#ig/RawRouter");

    app.use("/raw", new RawRouter().router);

    return new Promise((resolve) => {
        server.listen(3011, () => {
            resolve();
        });
    });
};

const stop = async () => {
    server.close();
};

module.exports = {
    start,
    stop,
};
